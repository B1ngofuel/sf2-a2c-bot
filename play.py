import pygame
import retro
import retrowrapper
from stable_baselines.common.policies import MlpPolicy, MlpLstmPolicy, MlpLnLstmPolicy, CnnLnLstmPolicy, CnnPolicy, CnnLstmPolicy
from stable_baselines.common.vec_env import SubprocVecEnv, DummyVecEnv
from stable_baselines import PPO2, A2C

gamename = "StreetFighterIISpecialChampionEdition-Genesis"
modelname = ("a2c-" + gamename)
model = A2C.load(modelname)
model2 = A2C.load(modelname)

env = retrowrapper.RetroWrapper(gamename, state='2p', players=2)
env2 = SubprocVecEnv([lambda: retro.make(gamename, state='2p')])
env3 = SubprocVecEnv([lambda: retro.make(gamename, state='2p')])


model.set_env(env2)
model2.set_env(env3)

obs = env.reset()

pygame.init()
win = pygame.display.set_mode((800,600))

j = pygame.joystick.Joystick(0)
j.init()

clock = pygame.time.Clock()

butts = ['B', 'A', 'MODE', 'START', 'UP', 'DOWN', 'LEFT', 'RIGHT', 'C', 'Y', 'X', 'Z']

action_array = [0,0,0,0,0,0,0,0,0,0,0,0]

while True:
    
    actions = set()
    
    # Display
    img = pygame.image.frombuffer(obs.tostring(), obs.shape[1::-1], "RGB")
    img = pygame.transform.scale(img, (800,600))
    win.blit(img,(0,0))
    pygame.display.flip()
    
    # Control Events
    
    pygame.event.pump()
    keys = pygame.key.get_pressed()
    
    for event in pygame.event.get():
        
        if keys[pygame.K_RIGHT] or j.get_hat(0) == (1,0):
            actions.add('RIGHT')
        if keys[pygame.K_LEFT] or j.get_hat(0) == (-1,0):
            actions.add('LEFT') 
        if keys[pygame.K_DOWN] or j.get_hat(0) == (0,-1):
            actions.add('DOWN')
        if keys[pygame.K_UP] or j.get_hat(0) == (0,1):
            actions.add('UP')            
        if j.get_hat(0) == (1,1):
            actions.add('RIGHT')
            actions.add('UP')
        if j.get_hat(0) == (-1,1):
            actions.add('LEFT')
            actions.add('UP')
        if j.get_hat(0) == (1,-1):
            actions.add('RIGHT')
            actions.add('DOWN')
        if j.get_hat(0) == (-1,-1):
            actions.add('LEFT')
            actions.add('DOWN')            
        if keys[pygame.K_z] or j.get_button(0):
            actions.add('A')
        if keys[pygame.K_x] or j.get_button(1):
            actions.add('B')
        if keys[pygame.K_c] or j.get_button(8):
            actions.add('C')
        if keys[pygame.K_a] or j.get_button(3):
            actions.add('X')
        if keys[pygame.K_s] or j.get_button(4):
            actions.add('Y')
        if keys[pygame.K_d] or j.get_button(9):
            actions.add('Z')

    
        for i, a in enumerate(butts):
            if a in actions:
                action_array[i] =  1            
            else:
                action_array[i] = 0
            
    a2, _ = model.predict(obs)
    a3, _ = model2.predict(obs)
    
    #act = a2.tolist() + action_array
    act = a2.tolist() + a3.tolist()
    
    # Progress Environemt forward
    obs, rew, done, info = env.step(act)
    
    clock.tick(60)
