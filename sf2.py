#lots of comments in this file. good luck :D 

import retro

from stable_baselines.common.policies import MlpPolicy, MlpLstmPolicy, MlpLnLstmPolicy, CnnLnLstmPolicy, CnnPolicy, CnnLstmPolicy
from stable_baselines.common.vec_env import SubprocVecEnv, DummyVecEnv
from stable_baselines import PPO2, A2C
import numpy as np
import gym

# sts is a list of save states that face Ryu (r) vs another character like e for E-Honda, b-Blanka, etc. The reason i have this list is because I iterate training through each player so that my model learns how to fight everyone. if you don't do this, he's likely to get confused by colours/playstyles and lose. 
sts = ['re',
        'rb',
        'rg',
        #'rcar',
        'rk',
        'rc',
        #'rbricks',
        'rd',
        'rr',
        'rbal',
        #'rbarrels',
        'rv',
        'rs',
        'rm',
        'rz']

# gamename is the name of the folder that has the rom.md for StreetfighterIISpecialChampionEdition on the Genesis 
gamename = 'StreetFighterIISpecialChampionEdition-Genesis'

# if you're using SubprocVecEnv , you can specify how many simulataneous environments are being trained on. 4 cpus, 4 envs. If you dont have 4 cpus, like if youre on a quad core or less, you should set this lower. if it's giving you weird errors, just use 1
n_cpu = 4

# this is just a variable name to save/load the trained model
modelname = ("a2c-" + gamename)

# A2C.load will load a PREEXISTING saved model. When you first start, you should not use this. This is for after you've already saved a model and you want to load the model to continue training, or something. I'm going to comment it out because I'm going to set this up for the FIRST training session
# model = A2C.load(modelname)

# SubprocVecEnv is for training in multiple environments simulatenously. My original code used 4 simulataneous environments because it's faster and I have 12 cores and using 4 of them won't affect my computer much. If you're having trouble with SubprocVecEnv, you can lower the number of CPUS or you can just use DummyVecEnv. 
# For simplicity, let's just use DummyVecEnv. I've commented out the SubprocVecEnv so you'll have to do this in your code if you haven't already
# env = SubprocVecEnv([lambda: retro.make(gamename, state='re') for i in range(n_cpu)])

# below is a DummyVecEnv. gamename is the game name and in this case, I only use state='re', which is Ryu vs E-Honda, to start the training process. The reason is that if you try to put the model = A2C(CnnPolicy, env, n_steps=128, verbose=1) inside a for loop to iterate through the save states, the whole thing errors. So this first training is just to set all that up.
env = DummyVecEnv([lambda: retro.make(gamename, state='re')])

# if you previously loaded the model with model = A2C.load, you would now have to set the new model. But we're starting from scratch so we don't need to set the model here so I commented it out.
#model.set_env(env)

# since we're starting a brand new model, and not loading one, we use the next line where we specify the model details. If we were loading a model, we wouldn't use this line, instead we'd use model = A2C.load (which would load all the model details)
model = A2C(CnnPolicy, env, n_steps=128, verbose=1)   

# this starts the learning. I have it set for 10000, but use whatever you want. The longer, the more he'll learn.
model.learn(total_timesteps=1000)

# Once learning is done, I save the model to the modelname
model.save(modelname)

# then I close the environment because I'm about to enter a loop where I iterate through the state names and learn on each one. and i'll be making a new env for each one (but using the same model)
env.close()


# start of my loop.
for st in sts:
    print(st)
    # Create the new environment with the new save state
    env = DummyVecEnv([lambda: retro.make(gamename, state=st, scenario='scenario')])
    # set the new env to the existing model
    model.set_env(env)
    # learn for a while
    model.learn(total_timesteps=100000)
    # save the model 
    model.save(modelname)
    # close the model and move on to the next state.
    env.close()

input("Hit Enter")
# load an easy save state for our demonstration (but you can load whatever state you want)
env = DummyVecEnv([lambda: retro.make('StreetFighterIISpecialChampionEdition-Genesis', state='re-easy', record='.')]) # for i in range(n_cpu)])    
# set the model to the environment
model.set_env(env)
# initate the environemnt 
obs = env.reset()

# while not 'done' (in the gym-retro sense that means you haven't lost the fight), predict and fight!
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    if dones.all() == True:
        break
    

 
