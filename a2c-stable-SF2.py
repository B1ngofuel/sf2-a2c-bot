import retro

from stable_baselines.common.policies import MlpPolicy, MlpLstmPolicy, MlpLnLstmPolicy, CnnLnLstmPolicy, CnnPolicy, CnnLstmPolicy
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines import PPO2, A2C
import numpy as np
import gym


class Discretizer(gym.ActionWrapper):
    """
    Wrap a gym-retro environment and make it use discrete
    actions for the Sonic game.
    """
    def __init__(self, env):
        super(Discretizer, self).__init__(env)
        #[1,0,0,0,0,0,0,0,0,0,0,0],[1,0,0,0,0,0,0,1,0,0,0,0],[1,0,0,0,0,0,1,0,0,0,0,0]]
        buttons = ["B", "A", "SELECT", "START", "UP", "DOWN", "LEFT", "RIGHT", "C", "Y", "X", "Z"] #not correct yet but good enough for FZero
        #actions = [['B'], ['LEFT', 'B'], ['RIGHT', 'B']]
        actions = [['LEFT'], ['RIGHT'], ['LEFT', 'DOWN'], ['RIGHT', 'DOWN'], ['DOWN'],
                   ['LEFT', 'UP'], ['RIGHT', 'UP'],['UP']]
        self._actions = []
        for action in actions:
            arr = np.array([False] * 12)
            for button in action:
                arr[buttons.index(button)] = True
            self._actions.append(arr)
        self.action_space = gym.spaces.Discrete(len(self._actions))
        #print(self.action_space)

    def action(self, a): # pylint: disable=W0221
        #print(self._actions[a].copy())
        return self._actions[a].copy()

#sts = ['ryuchunli', 'ryublank', 'ryuehonda', 'ryuguile', 'ryuken', 'ryucar'] #, 'Level1p2']

sts = ['re',
        'rb',
        'rg',
        #'rcar',
        'rk',
        'rc',
        #'rbricks',
        'rd',
        'rr',
        'rbal',
        #'rbarrels',
        'rv',
        'rs',
        'rm',
        'rz']

gamename = 'StreetFighterIISpecialChampionEdition-Genesis'


n_cpu = 4

modelname = ("a2c-" + gamename)
model = A2C.load(modelname)

env = SubprocVecEnv([lambda: retro.make(gamename, state='re') for i in range(n_cpu)])
model.set_env(env)
#model = A2C(CnnPolicy, env, n_steps=128, verbose=1)   

#model.learn(total_timesteps=1000000)
#model.save(modelname)
#env.close()
#for i in range(5):
for st in sts:
    print(st)
    env = SubprocVecEnv([lambda: retro.make(gamename, state=st, scenario='scenario') for i in range(n_cpu)])    
    model.set_env(env)
    model.learn(total_timesteps=100000)
    model.save(modelname)
    env.close()

#env = SubprocVecEnv([lambda: retro.make('DonkeyKongCountry-Snes', state=sts[i]) for i in range(len(sts))])    
#model = A2C(CnnPolicy, env, n_steps=128, verbose=1)   
#model.set_env(env)



input("Hit Enter")
#env = retro.make('FZero-Snes', state='go')
#env = retro.make(gamename, state=state, scenario='scenario2')
#env1 = SubprocVecEnv([lambda: env for i in range(n_cpu)])
#model.set_env(env1)
env = SubprocVecEnv([lambda: retro.make('StreetFighterIISpecialChampionEdition-Genesis', state='re-easy', record='.')]) # for i in range(n_cpu)])    

#env = SubprocVecEnv([lambda: retro.make('Tennis-Nes', state=state, scenario='scenario2') for i in range(n_cpu)])    
model.set_env(env)
obs = env.reset()
print(obs.shape)



while True:
    action, _states = model.predict(obs)
    #print(action)
    obs, rewards, dones, info = env.step(action)
    #print(dones)
    env.render()
    #if dones:
    if dones.all() == True:
        #obs = env.reset()
        break
    

